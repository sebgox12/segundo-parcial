using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SP_EJ2
{
    public class Producto
    {
        string nombre;
        int precio;
        int[] existencia = { 0, 10, 10, 10, 10, 10 };

        public Producto(string N, int P)
        {
            nombre = N;
            precio = P;
        }

        public int Cantidad(int i,int M)
        {
            
            if (existencia[i] > 0)
            {
                int D = M - precio;

                Console.WriteLine("Le sobran " + D);

                Console.WriteLine("ha seleccionado un {0} por {1}", nombre, precio);

                existencia[i]--;

                int cantRes = existencia[i];

                Console.WriteLine("quedan " + cantRes);
            }
            return existencia[i];
        }
       
    }

    class Program
    {
        static void Main(string[] args)
        {
            Producto Doritos = new Producto("doritos",15);

            Producto Agua = new Producto("planeta azul",20);

            Producto Galleta = new Producto("Emperador",10);

            Producto Yogurt= new Producto("Yogurt de fresa",50);

            Producto Sandwich = new Producto("Sandwich", 120);

            int billetes = 0;
            int monedas = 0;

            int CBilletes = 1;
            int CMonedas = 1;

            while (1 < 2)
            {
                Console.WriteLine("Selecciones\n 1 doritos(15 pesos)\n 2 agua planeta azul(20 pesos)" +
                    "\n 3 galleta emperador(10 pesos)\n 4 yogurt de fresa(50 pesos)\n sandwich(120 pesos)");
                short op = short.Parse(Console.ReadLine());

                Console.WriteLine("Pagara con billetes(50,100,200). Si no desea insertar billetes pulse 0");
                billetes = int.Parse(Console.ReadLine());
                if(billetes != 0)
                {
                    Console.WriteLine("Que cantidad de billetes desea insertar");
                    CBilletes = int.Parse(Console.ReadLine());
                }

                Console.WriteLine("Pagara con monedas(5,10,25). Si no desea insertar monedas pulse 0");
                monedas = int.Parse(Console.ReadLine());
                if(monedas != 0)
                {
                    Console.WriteLine("Que cantidad de monedas desea insertar");
                    CMonedas = int.Parse(Console.ReadLine());
                }

                int monto = (billetes*CBilletes) + (CMonedas*monedas);

                if ((billetes == 50 || billetes == 100 || billetes == 200 || billetes == 0) &&
                (monedas == 0 || monedas == 5 || monedas == 10 || monedas == 25))
                {
                    switch (op)
                    {
                        case 1:

                            if (monto >= 15)
                            {
                                if (Doritos.Cantidad(op,monto) > 0)
                                {
                                    
                                }
                                else
                                {
                                    Console.WriteLine("Producto no disponible");
                                }                            
                            }
                            else
                            {
                                Console.WriteLine("Monto insuficiente");
                            }

                            break;

                        case 2:

                            if (monto >= 20)
                            {
                                if (Agua.Cantidad(op, monto) >= 0)
                                {

                                }
                                else
                                {
                                    Console.WriteLine("Producto no disponible");
                                }
                            }
                            else
                            {
                                Console.WriteLine("Monto insuficiente");
                            }
                            break;

                        case 3:

                            if (monto >= 10)
                            {
                                if (Galleta.Cantidad(op, monto) > 0)
                                {

                                }
                                else
                                {
                                    Console.WriteLine("Producto no disponible");
                                }
                            }
                            else
                            {
                                Console.WriteLine("Monto insuficiente");
                            }

                            break;

                        case 4:

                            if (monto >= 50)
                            {
                                if (Yogurt.Cantidad(op, monto) > 0)
                                {

                                }
                                else
                                {
                                    Console.WriteLine("Producto no disponible");
                                }
                            }
                            else
                            {
                                Console.WriteLine("Monto insuficiente");
                            }

                            break;

                        case 5:

                            if (monto >= 120)
                            {
                                if (Sandwich.Cantidad(op, monto) > 0)
                                {

                                }
                                else
                                {
                                    Console.WriteLine("Producto no disponible");
                                }
                            }
                            else
                            {
                                Console.WriteLine("Monto insuficiente");
                            }

                            break;


                        default:
                            Console.WriteLine("Opcion no valida");
                            break;
                    }
                }

                else { Console.WriteLine("Cantidad invalida");}

                Console.WriteLine("-------------------------------------------------------------------------");
            }
           
        }
    }
}
